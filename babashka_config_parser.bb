(ns babashka-config-parser)

(require '[clojure.string :as str])
(require '[babashka.deps :as deps])

;(deps/add-deps '{:deps
 ;                {babashka-tree/babashka-tree {:local/root "babashka-utility"}}})

(require '[babashka-utility :as util])
(require '[babashka-tree :as tree])

(def cli-options
  ;; Defined CLI options here
  [["-s" "--search SEARCH-TERM" "Regex to search for in the EXIF data"]
   ["-d" "--dir DIR"]
   ["-e" "--exclude EXCLUDE" "Regex to exclude"]
   ["-i" "--insensitive" "Case insensitive"] ;;Unimplemented
   ["-c" "--count" "Include number of files"] ;; unimplemented
   ["-o" "--out-name OUTNAME" "Output file name"]]) ;;unimplemented

;;(defstruct tree :parent :value) ;; We need a pointer to 

(defn remaining-lines-to-parse
  "Given a subtree of parsed lines and the number of lines pre-subtree, determine which lines are left to parse"
  [subtree lines]
  (take-last (- (count lines) (tree/count-tree-nodes subtree)) lines))

(defn simple-tree-structure
  "Create a tree structure for whenever there is only a start or end rather than both"
  [delims conf-str]
  (str/split conf-str (re-pattern (str/join "|" delims)))) ;; '|' is the or symbol, so we match any item in the list

(defn generate-tree-structure-main
  "Generate a backwards-ordered tree"
  [start-tags end-tags lines tree]
  ;;(println "Lines: " (count lines))
  (if (util/words-equal-any? (first lines) start-tags)
    (let [subtree (tree/tree->add-branch (generate-tree-structure-main start-tags end-tags (rest lines) nil) (first lines))]
      (recur start-tags end-tags
             (remaining-lines-to-parse subtree lines) (tree/tree->add-branch tree subtree)))
    (if (util/words-equal-any? (first lines) end-tags)
      (reverse (tree/tree->add-branch tree (first lines))) ;; Since conj adds to the beginning of seqs, we reverse every subtree when we are done with it to get it in order
      (recur start-tags end-tags (rest lines) (tree/tree->add-branch tree (first lines))))))

(defn generate-tree-structure
  "Generate a correctly ordered tree"
  [start-tags end-tags lines tree]
  (generate-tree-structure-main start-tags end-tags lines tree))

(defn generate-block-tree
  "Generate a tree based on start/stop syntax"
  [start-tags end-tags conf-str]
  (if (or (empty? start-tags) (empty? end-tags));;If there are only start or end tags
    (simple-tree-structure
     (if (empty? end-tags) start-tags end-tags) 
     conf-str)
    (generate-tree-structure start-tags end-tags (util/trim-all (str/split-lines conf-str)) nil)))

(defn -main
  []
  (let [tree (generate-block-tree nil ["end" "next"] "Hello end I hate you end wow end")
        users-config (tree/get-branch-contains tree "config user local")
        users (tree/get-branch-contains users-config (list "edit" "two-factor"))
        users-tokens (tree/filter-tree-surface (first users) (list "edit" "two-factor"))
        users-with-two-factor (filter #(some (fn [s] (str/includes? s "two-factor")) %) users-tokens)]
    tree))

;;
(-main)
